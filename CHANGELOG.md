
# Changelog

## [v1.2.2] - 2021-01-21

### Features

- update repository declaration to new nexus url
- update seadatanet-connector lower bound range
- update ecological-engine-geospatial-extension lower bound range

## [v1.2.1] [r4.24.0] - 2020-06-10

### Features

- Updated for support https protocol [#19423]



## [v1.2.0] - 2016-09-27

### Features

- Better management of dependencies and removal of obsolete algorithms



## [v1.1.4] - 2015-09-23

### Features

- Adding algorithms by univ Brazil



## [v1.1.3] - 2015-06-30

### Features

- Introducing average annual precipitation



## [v1.1.2] - 2014-09-05

### Features

- Geothermal analysis



## [v1.1.1] - 2013-10-22

### Features

- Fix problem in species/taxa procedures



## [v1.1.0] - 2013-07-22

### Features

- Add TrendyLyzer algorithms and species/taxa procedures



## [v1.0.0] - 2013-03-19

### Features

- First Release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
